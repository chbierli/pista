from glauber import *

# basic usage pPb
#g = BlackDisk("2212","1000822080",67.9)
g = BlackDisk("1000822080","1000822080",67.9)

parts,subcols = g.next(2.0)

# print event record
#for ps in parts:
#    print ps.printToLine()

print "..."

for ss in subcols:
    print ss.printToLine()

print "..."
sortcols = sorted(subcols, key = lambda k : k["scale"], reverse=True)

absp1 = []
absp2 = []
rem1 = []
rem2 = []

# backbone interactions
for ss in sortcols:
    i1 = ss["i1"]
    i2 = ss["i2"]
    if i1 not in absp1 and i2 not in absp2:
        absp1.append(i1)
        absp2.append(i2)
    else:
        rem1.append(i1)
        rem2.append(i2)

    
# make some histograms

from yoda import Histo1D
ncoll = Histo1D(200,0,200,"/nwound") 
impact = Histo1D(10,0,50,"/impact")

for i in range(0,10):
    if i%100 == 0:
        print i
    p,s = g.next(28.0)
    if len(s) > 0:
        ncoll.fill(len(s) + 1,g.w)
        impact.fill(g.b,g.w)

ncoll.normalize()

import matplotlib.pyplot as plt
def convertYoda(h):
    x = []
    y = []
    for b in h:
        x.append(b.xMid)
        y.append(b.sumW)
    return x,y

f, ax = plt.subplots(1,1)
x,y = convertYoda(impact)
ax.plot(x,y)
f.savefig("impact.ps")
ax.clear()
x,y = convertYoda(ncoll)
ax.set_yscale("log")
ax.plot(x,y)
f.savefig("ncoll.ps")

