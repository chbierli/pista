#include "Rivet/Analysis.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "AlicePrimary.hh"
#include "Centrality.h"

// Analysis to reproduce ALICE strangeness observables from ALICE_2016_I1471838.
// Need to run plotCOLOBS.py afterburner to produce sensible plots.
// Author: Christian Bierlich, bierlich@thep.lu.se
//

namespace Rivet {

  class ALICE_2016_I1471838 : public Analysis {
  public:

    /// Constructor
    ALICE_2016_I1471838()
      : Analysis("ALICE_2016_I1471838")
    {    }


    //@{

    void init() {
      double etamax = 0.5;
      // Central multiplicity 
      ChargedFinalState cfsMult( Cuts::abseta < etamax);
      addProjection(cfsMult, "CFSMult");
      // Centrality estimator
      ChargedFinalState cfsCent( ((Cuts::eta > 2.8 && Cuts::eta < 5.1) || (Cuts::eta > -3.7 && Cuts::eta < 1.7)));
      addProjection(cfsCent, "CFSCent");
      // Projection of identified particles
      Primary ufs(Cuts::absrap < etamax);
      addProjection(ufs,"UFS");
      // Setting up centrality classes and estimators
      Histo1DPtr cHist1 = bookHisto1D("cHist1",100,0,500);
      Histo1DPtr cHist2 = bookHisto1D("cHist2",100,0,500);
      Histo1DPtr cHist3 = bookHisto1D("cHist3",100,0,500);
      Histo1DPtr cHist4 = bookHisto1D("cHist4",100,0,500);
      Histo1DPtr cHist5 = bookHisto1D("cHist5",100,0,500);
      Histo1DPtr cHist6 = bookHisto1D("cHist6",100,0,500);
      Histo1DPtr cHist7 = bookHisto1D("cHist7",100,0,500);
      Histo1DPtr cHist8 = bookHisto1D("cHist8",100,0,500);
      Histo1DPtr cHist9 = bookHisto1D("cHist9",100,0,500);
      cNc = CentralityHistogram<double>("cent1",100,0,500,cHist1);
      cNc5 = CentralityHistogram<double>("cent9",100,0,500,cHist9);
      cPi5 = CentralityHistogram<double>("cent2",100,0,500,cHist2);
      cPi10 = CentralityHistogram<double>("cent3",100,0,500,cHist3);
      cKa = CentralityHistogram<double>("cent4",100,0,500,cHist4);
      cLa = CentralityHistogram<double>("cent5",100,0,500,cHist5);
      cXi = CentralityHistogram<double>("cent6",100,0,500,cHist6);
      cOm = CentralityHistogram<double>("cent7",100,0,500,cHist7);
      cPr = CentralityHistogram<double>("cent8",100,0,500,cHist8);
      cNc5 = CentralityHistogram<double>("cent9",100,0,500,cHist9);
      vector<double>centData10 = {0.00, 0.0095, 0.047, 0.095, 0.14, 0.19, 0.28, 0.38, 0.48, 0.68, 1.00};
      // Omega has only 5 centrality classes
      vector<double>centData5 = {0.00, 0.047, 0.14, 0.28, 0.48, 1.00};
      for(size_t i = 0; i < centData10.size() - 1; ++i ) {
	stringstream histname;
	histname << "nCh_" << i; 
        cNc.addHistogram(bookHisto1D(histname.str().c_str(),100,0,50),centData10[i],centData10[i+1]);
	histname.clear();
	histname.str("");
	histname << "nPi10_" << i;
	cPi10.addHistogram(bookHisto1D(histname.str().c_str(),100,0,50),centData10[i],centData10[i+1]);
	histname.clear();
	histname.str("");
	histname << "nPr_" << i;
	cPr.addHistogram(bookHisto1D(histname.str().c_str(),100,0,50),centData10[i],centData10[i+1]);
	histname.clear();
	histname.str("");
	cKa.addHistogram(bookHisto1D(i+1,1,1),centData10[i],centData10[i+1]);
	cLa.addHistogram(bookHisto1D(10+i+1,1,1),centData10[i],centData10[i+1]);
	cXi.addHistogram(bookHisto1D(20+i+1,1,1),centData10[i],centData10[i+1]);
      }
      for(size_t i = 0; i < centData5.size() - 1; ++i) {
        stringstream histname;
	histname << "nPi5_" << i;
	cPi5.addHistogram(bookHisto1D(histname.str().c_str(),100,0,50),centData5[i],centData5[i+1]);
	histname.clear();
	histname.str("");
	histname << "nCh5_" << i;
	cNc5.addHistogram(bookHisto1D(histname.str().c_str(),100,0,50),centData5[i],centData5[i+1]);
	cOm.addHistogram(bookHisto1D(30+i+1,1,1),centData5[i],centData5[i+1]);
      }
      
      // booking histos
    }


  void analyze(const Event& event) {
	  const double weight = event.weight();
	  const ChargedFinalState& cfsMult = applyProjection<ChargedFinalState>(event, "CFSMult");
	  const ChargedFinalState& cfsCent = applyProjection<ChargedFinalState>(event, "CFSCent");
	  const Primary& ufs = applyProjection<Primary>(event, "UFS");

	  // Require at least one forward and one central particle
	  if (cfsMult.particles().size() < 1 || cfsCent.particles().size() < 1)
		  vetoEvent;
	  // Forward mult is centrality estimator
	  double fm = cfsCent.particles().size();
	  cNc.eventFill(weight,fm);
	  cNc5.eventFill(weight,fm);
	  cPi5.eventFill(weight,fm);
	  cPi10.eventFill(weight,fm);
	  cKa.eventFill(weight,fm);
	  cLa.eventFill(weight,fm);
	  cXi.eventFill(weight,fm);
	  cOm.eventFill(weight,fm);
	  cPr.eventFill(weight,fm);

	  for (const Particle&p : ufs.particles()) {
	    double pT = p.pT()/GeV;
	    int id = abs(p.pid());
	    cNc.fill(pT,weight,fm);
	    cNc5.fill(pT,weight,fm);

	    if(id == 211) {
	      cPi5.fill(pT,weight,fm);
	      cPi10.fill(pT,weight,fm);
	    }
	    else if(id == 2212) {
	      cPr.fill(pT,weight,fm);
	    }
	    else if(id == 310) {
	      cKa.fill(pT,weight,fm);
	    }
	    else if(id == 3122) {
	      cLa.fill(pT,weight,fm);
	    }
	    else if(id == 3312) {
	      cXi.fill(pT,weight,fm);
	    }
	    else if(id == 3334) {
	      cOm.fill(pT,weight,fm);
	    }
	  
	  }
}


    void finalize() {
	cNc.finalize();
	cNc5.finalize();
	cPi5.finalize();
	cPi10.finalize();
	cPr.finalize();
	cKa.finalize();
	cLa.finalize();
	cXi.finalize();
	cOm.finalize();

    }

    //@}


  private:
    CentralityHistogram<double> cNc; 
    CentralityHistogram<double> cNc5; 
    CentralityHistogram<double> cPi10; 
    CentralityHistogram<double> cPi5; 
    CentralityHistogram<double> cKa; 
    CentralityHistogram<double> cPr; 
    CentralityHistogram<double> cLa; 
    CentralityHistogram<double> cXi; 
    CentralityHistogram<double> cOm; 

  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ALICE_2016_I1471838);

}
