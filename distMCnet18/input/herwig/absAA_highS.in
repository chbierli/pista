# -*- ThePEG-repository -*-

################################################################################
# This file contains our best tune to UE data from ATLAS at 7 TeV. More recent
# tunes and tunes for other centre-of-mass energies as well as more usage
# instructions can be obtained from this Herwig wiki page:
# http://projects.hepforge.org/herwig/trac/wiki/MB_UE_tunes
# The model for soft interactions and diffractions is explained in
# [S. Gieseke, P. Kirchgaesser, F. Loshaj, arXiv:1612.04701]
################################################################################

read snippets/PPCollider.in

##################################################
# Technical parameters for this run
##################################################
cd /Herwig/Generators
##################################################
# LHC physics parameters (override defaults here) 
##################################################
set EventGenerator:EventHandler:LuminosityFunction:Energy 5020.

# Intrinsic pT tune extrapolated to LHC energy
set /Herwig/Shower/ShowerHandler:IntrinsicPtGaussian 2.2*GeV

# Minimum Bias
read snippets/MB.in

read snippets/SoftTune.in

# Diffraction model
read snippets/Diffraction.in


cd /Herwig/MatrixElements/

clear QCDDiffraction:MatrixElements
clear /Herwig/Generators/EventGenerator:EventHandler:SubProcessHandlers

insert /Herwig/Generators/EventGenerator:EventHandler:SubProcessHandlers[0] QCDMinBias


set /Herwig/Cuts/MinBiasCuts:X1Min 0.11
set /Herwig/Cuts/MinBiasCuts:X2Min 0.11


cd /Herwig/Generators

insert EventGenerator:AnalysisHandlers 0 /Herwig/Analysis/HepMCFile
set /Herwig/Analysis/HepMCFile:PrintEvent 1000000000
set /Herwig/Analysis/HepMCFile:Format GenEvent
set /Herwig/Analysis/HepMCFile:Units GeV_mm
set /Herwig/Analysis/HepMCFile:Filename absAA_highS.fifo

##################################################
# Save run for later usage with 'Herwig run'
##################################################
saverun absAA_highS EventGenerator

