#!/bin/sh
#SBATCH -t 09:00:00
# specify project
#BATCH -A hep2016-1-5
# specify storage
#SBATCH -p hep
HOME=$PWD
source /home/jbellm/opt/bin/activate
#export RIVET_ANALYSIS_PATH=/lunarc/nobackup/users/jbellm/hiwig/hg/rivet-analyses
cd input/herwig
rm -f *.fifo
mkfifo sigAA.fifo
mkfifo absAA.fifo
mkfifo difAA1.fifo
mkfifo difAA2.fifo

Herwig read sigAA.in
Herwig read absAA.in
Herwig read difAA1.in
Herwig read difAA2.in

Herwig run sigAA.run  -s$RANDOM -N1000000000 &> H7_sigAA.log &
RUNNING_PID0=$!
Herwig run absAA.run  -s$RANDOM -N1000000000 &> H7_absAA.log &
RUNNING_PID1=$!
Herwig run difAA1.run -s$RANDOM -N1000000000 &> H7_difAA1.log &
RUNNING_PID2=$!
Herwig run difAA2.run -s$RANDOM -N1000000000 &> H7_difAA2.log &
RUNNING_PID3=$!



cd $HOME/input
rm -f *fifo
ln -s herwig/sigAA.fifo sig.fifo
ln -s herwig/absAA.fifo abs.fifo
ln -s herwig/difAA1.fifo dif1.fifo
ln -s herwig/difAA2.fifo dif2.fifo
cd $HOME

export PYTHONPATH=/home/jbellm/.local/lib/python2.7/site-packages/:$PYTHONPATH
python pista.py 4

kill -9 $RUNNING_PID0
kill -9 $RUNNING_PID1
kill -9 $RUNNING_PID2
kill -9 $RUNNING_PID3

(cd input;rm   *pyc *fifo *run *log *out)
rm lib/*pyc
