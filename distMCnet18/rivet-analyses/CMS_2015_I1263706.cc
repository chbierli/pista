// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "YODA/ReaderYODA.h"
// Pt spectra to 120 GeV in pPb collisions

namespace Rivet {


  class CMS_2015_I1263706 : public Analysis {
  public:

    /// Constructor
    CMS_2015_I1263706()
      : Analysis("CMS_2015_I1263706")
    {    }


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

	ChargedFinalState cfs(Cuts::abseta < 1.8 && Cuts::pT > 0.4*GeV);
	addProjection(cfs,"CFS");
	
	_h_eta1 = bookHisto1D(1,1,1);
	_h_eta2 = bookHisto1D(2,1,1);
	_h_eta3 = bookHisto1D(3,1,1);
	_h_eta4 = bookHisto1D(4,1,1);
	_h_eta5 = bookHisto1D(5,1,1);
	_h_eta6 = bookHisto1D(6,1,1);
	_h_eta7 = bookHisto1D(7,1,1);
	
	
	sow = 0;
	nCavg = 0;

    }


    /// Normalise histograms etc., after the run
    void analyze(const Event& event) {
      const ChargedFinalState& cfs = applyProjection<ChargedFinalState>(event, "CFS");
  
      const double weight = event.weight();
      sow +=weight;
      for (const Particle& p : cfs.particles()){
	const double pT = p.pT()/GeV;
	const double eta = p.eta();
	if(fabs(eta) < 1.0 )
		_h_eta1->fill(pT,weight/2/M_PI/pT/2.0);
	if(eta < -1.3 && eta > -1.8 ) 
		_h_eta2->fill(pT,weight/2/M_PI/pT/0.5);
	if(eta > -1.3 && eta < -0.8 )
		_h_eta3->fill(pT,weight/2/M_PI/pT/0.5);
	if(eta > -0.8 && eta < -0.3 )
		_h_eta4->fill(pT,weight/2/M_PI/pT/0.5);
	if(eta > 0.3 && eta < 0.8 )
		_h_eta5->fill(pT,weight/2/M_PI/pT/0.5);
	if(eta > 0.8 && eta < 1.3 )
		_h_eta6->fill(pT,weight/2/M_PI/pT/0.5);
	if(eta > 1.3 && eta < 1.8 )
		_h_eta7->fill(pT,weight/2/M_PI/pT/0.5);
      }


    }



    void finalize() {
     
     	nCavg/=sow;
     	scale(_h_eta1,1./sow);
     	scale(_h_eta2,1./sow);
     	scale(_h_eta3,1./sow);
     	scale(_h_eta4,1./sow);
     	scale(_h_eta5,1./sow);
     	scale(_h_eta6,1./sow);
     	scale(_h_eta7,1./sow);

}
  private:

    // Data members like post-cuts event weight counters go here
    Histo1DPtr _h_eta1, _h_eta2, _h_eta3, _h_eta4, _h_eta5, _h_eta6, _h_eta7;
    double sow;
    double nCavg;


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(CMS_2015_I1263706);


}
