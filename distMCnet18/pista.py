import sys
sys.path.insert(0,'lib/')
from hitools import *
from crosssec import *


############################################
############### EASY SETUP #################
############################################

# For some standardized collision systems,
# setup of the Glauber calculation, setup of .fifo pipes
# which RIVET analyses to use, etc are pre-prepared.
# Fill in the global variables below appropriately.
# For advanced use (own analyses...) they can be modified.
# For even more advanced use, modify the "simulate" function 
# below, as well as the "setupFifo" function in hitools.py.

projectile = "1000822080"
target = "1000822080"
mywidth = 5.

# The .fifo paths should correspond to the generator output.
# Here sensible defaults are given.
absFifo = "input/abs.fifo"
dif1Fifo = "input/dif1.fifo"
dif2Fifo = "input/dif2.fifo"
sigFifo = None
rivetAnalyses = []

# Use fluctuating disks in Glauber calculation
# instead of Black Disks. Default parameters
# are for 5.02 TeV/nn
gribovDisks = True

# Possibility of enhancing Glauber Black disk
# cross section calculated with Schuler-Sjostrand
# with a constant factor.
xsecFactor = 1.

def easySetup(syst):
    global rivetAnalyses 
    global mywidth
    if syst == '1':
        # Collide p with Pb at sqrt{s} = 5.02 TeV/nn
        global projectile 
        projectile = "2212"
        rivetAnalyses = ["CMS_2015_I1263706","ATLAS_2015_I1386475"]
        global dif2Fifo 
        dif2Fifo = None
        mywidth = 4.

    elif syst == '2':
        # Collide Pb with Pb at sqrt{s} = 2.76 TeV/nn
        rivetAnalyses = ["ALICE_2013_I1225979","ALICE_2016_I1394676","ATLAS_2015_I1360290"]
        mywidth = 8.

    elif syst == '3':
        # Collide Pb with Pb at sqrt{s} = 5.02 TeV/nn
        rivetAnalyses = ["ALICE_2016_I1507090"]
        mywidth = 15.

    elif syst == '4':
        # Collide Pb with Pb at sqrt{s} = 5.02 TeV/nn
        # with a signal process specified.
        rivetAnalyses = ["HI_JET"]
        global sigFifo 
        sigFifo = "input/sig.fifo"
        mywidth = 5.

    else:
        print "Requested a system option which is not implemented. This will not end well..."
        sys.exit()


# Here the simulation itself is done.
from glauber import *

# Here the simulation it self is done.
def simulate():
    # Setup pipes for input...
    signal = None
    if sigFifo != None:
        signal = Pipe(sigFifo,'r')
    absorptive = Pipe(absFifo,'r')
    dif1 = Pipe(dif1Fifo,'r')
    dif2 = None
    if dif2Fifo != None:
        dif2 = Pipe(dif2Fifo,'r')
    # ...and output.
    rivetPipe = Pipe(".fifo.hepmc","w")
    # Get a test absorptive event to setup kinematics from
    # input, ecm, pplus and pminus.
    testevt = absorptive.getNext()

    vec0 = testevt.beam_particles()[0].momentum()
    vec1 = testevt.beam_particles()[1].momentum()

    pPlusMax = vec0.e() + vec0.pz()
    pMinusMax = vec1.e() - vec1.pz()
   
    # Assume e > m
    ecm = math.sqrt((vec0.e() + vec1.e())**2 - (vec0.pz() + vec1.pz())**2) 

    # Set or unset debug flag for extra printouts.
    debug = False
    
    # Calculate input pp cross section using SS scheme.
    c = CrossSections()
    c.calculate(ecm)
    sigmaNN = c.sigTot - c.sigEl - 2.*c.sigSD
    g = None
    if gribovDisks:
        print "Initializing Gribov-Glauber calculation with parameters suitable for 5 TeV/nn."
        g = GribovDisk(projectile,target,4.45,0.43)
    else:
        print "Initializing Glauber BD calculation with sigmaNN = ",xsecFactor*sigmaNN,"mb."
        g = BlackDisk(projectile,target,xsecFactor*sigmaNN)

    # Control histograms to be filled during the run can be
    # created here.
    hSub = yoda.Histo1D(100,0,50,"/control/subcols")
    hImpact = yoda.Histo1D(100,0,25,"/control/impact")
    hWeights = yoda.Histo1D(100,0,1000,"/control/weights")

    # Loop over number of events, change number as desired:
    Nevents=500
    # Some statistics:
    nohard          = 0. 
    analysed_events = 0.
    avg_hard        = 0.
    avg_additional  = 0.
    avg_adding_mom_cons  = 0.
    avg_adding_non_mom_cons = 0.
    
    for nevent in range(0,Nevents):
        if debug and nevent%5 == 0:
            print "Runnning event %s"%(str(nevent))
        if not debug and nevent%5 == 0:
            print "Runnning event %s"%(str(nevent))            
        # Get the next Glauber event.
        parts, subcols = g.next(mywidth)
        if debug:
            print "Glauber gives ",len(subcols),"subcollisions"
        if len(subcols) > 0:
            hSub.fill(len(subcols),g.w)
        hImpact.fill(g.b)
        hWeights.fill(g.w)
        # Sort subcollisions according to impact parameter.
        # scale is 1/b
        sortcols = sorted(subcols, key = lambda k : k["scale"], reverse=True)

        # Make lists of subcollisions. 
        hard = []
        additional_left = []
        additional_right = []
        additional_share = []
        used_left=[]
        used_right=[]
        
        # The subcollisions are ordered in scale!
        # In future work one can change this to choose 
        # according to 1/b.
        for ss in sortcols:
            i1 = ss["i1"]
            i2 = ss["i2"]
            # If non of the participans was used:
            # We have a new hard process!
            if i1 not in used_left and i2 not in used_right:
                hard+=[[i1,i2]]
                used_left+=[i1]
                used_right+=[i2]
            # If index 1 was not used proviously (index 2 was used already!)
            elif i1 not in used_left:
                additional_left+=[[i1,i2]]
                used_left+=[i1]
            # If index 2 was not used proviously (index 1 was used already!)
            elif i2 not in used_right:
                additional_right+=[[i1,i2]]
                used_right+=[i2]
            # Both indices have been used before:
            else:
                additional_share+=[[i1,i2]]

        # We need at least one backbone interaction
        if len(hard) < 1:
            nohard+=1
            continue
        hard             = np.array(hard)
        
        # add all for correlated collisiona
        
        additional_all   = np.array(additional_left+
                                    additional_right+
                                    additional_share)
        avg_additional+=len(additional_all)
        additional_left  = np.array(additional_left)
        additional_right = np.array(additional_right)
        additional_share = np.array(additional_share)
        if debug:
            print "(left,right,share)"
            print len(additional_left),len(additional_right),len(additional_share)
        # We let the first backbone interaction serve
        # as the primary event.
        if signal != None:
            nextevent = signal.getNext()
        elif absorptive:
            # get the number N of correlated collisions 
            # then choose the hardest out of N:
            N=correlatedCollisions( hard , additional_all , 0, 0 )
            tocheck=[[absorptive.getNext()] for it in range(N)]
            nextevent = selectHardest(tocheck,"HT")
            avg_hard+=1.
        else :
            print "Something went wrong. Did not get event from pipe."
        
        
        # Set up a list of subevents to be added.
        aevents = []

        for isub in range(1,len(hard)):
            avg_hard+=len(hard)
            N=correlatedCollisions( hard , additional_all , isub, 1 )
            tocheck=[[absorptive.getNext()] for it in range(N)]
            addthis = selectHardest(tocheck,"HT")
            aevents.append([addthis,hard[isub]])
            
        for add in additional_left:
            aevents.append([dif2.getNext(),add])
        for add in additional_right:
            aevents.append([dif1.getNext(),add])
        for add in additional_share:
            if random() < 0.5:  
                aevents.append([dif1.getNext(),add])
            else:    
                aevents.append([dif2.getNext(),add])
                
        aevents = selectHardest(aevents,"HT",True) 
        # Add the secondary events to the primary one.
        v1 = nextevent.vertices()[0]
        
        # For approximate momentum conservation:
        mom_info=getPlusMinus(nextevent,etaMax=7.)
        pPlus =np.zeros(2*g.At)
        pMinus=np.zeros(2*g.At)

        
        pPlus[hard[0][0] - 1]  = np.array(mom_info[0])
        pMinus[hard[0][1] - 1] = np.array(mom_info[1])
        for addevent in aevents:  
          mom_info_tmp = getPlusMinus(addevent[0],etaMax=7.)
          pPlus_t  = mom_info_tmp[0]
          pMinus_t = mom_info_tmp[1]
          avg_adding_non_mom_cons+=1.
          if abs(pPlus[addevent[1][0] - 1] + pPlus_t)   > pPlusMax:
                continue
          if abs(pMinus[addevent[1][1] - 1] + pMinus_t) > pMinusMax:
                continue
                
          pPlus [addevent[1][0] - 1]  += pPlus_t
          pMinus[addevent[1][1] -1] += pMinus_t

          avg_adding_mom_cons+=1.
            
          for p in addevent[0].fsParticles():
            v1.add_particle_out(p)
       
        
        # Pipe the event to rivet:
        cur_w=nextevent.weight()
        cur_w.setWeight(g.w)
        if debug:
            print "\rAnalyse:",nextevent.summary(),nextevent.weight()
        
        
        rivetPipe.pushNext(nextevent)
        analysed_events+=1
        # clean up the used memory.
        nextevent.clearSilent()
        for addevent in aevents:
          # clear the added event.
          addevent[0].clearSilent()
          
        import gc
        gc.collect()
    
    if debug:
        print "\n\nStatistics:"
        print "- No hard event in %s of %s events"%(nohard,Nevents)
        print "- %s analysed events"%analysed_events
        print "- On average %f hard events."%(avg_hard/analysed_events)
        print "- On average %f additional wounded events."%(avg_additional/analysed_events)
        print "- On average %f events were added with momentum conservation."%(avg_adding_mom_cons/analysed_events)
        print "- On average %f events would have been added without momentum conservation."%(avg_adding_non_mom_cons/analysed_events)
    
       
    hists = [hSub, hImpact, hWeights]
    from yoda import writeYODA
    writeYODA(hists,"testHisto.yoda")
    
    return


    print "Skipped "+str(skipped)+" events."
    hists = [hSub, hImpact, hWeights]
    from yoda import writeYODA
    writeYODA(hists,"testHisto.yoda")
    return


if __name__ == '__main__':
    sArg = '1'
    op = "pista"
    if len(sys.argv) < 2:
        print "No input argument specified. Defaulting to option 1, pPb at 5.02 TeV (or whatever you replaced it with)"
    else:
        sArg = str(sys.argv[1])
        if len(sys.argv) == 3:
            op = str(sys.argv[2])
    easySetup(sArg)
    ah = setupFifo(rivetAnalyses)
    sim = multiprocessing.Process(target=simulate)
    run = rivet.Run(ah)
    ana = multiprocessing.Process(target=analyse, args=[run,ah,op])
    sim.start()
    ana.start()
    sim.join()
    ana.join()
