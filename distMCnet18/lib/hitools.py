#! /usr/bin/env/python

## HEP packages:
import yoda, rivet, hepmc

## Standard Packages:
import os,math
import sys
import multiprocessing
import numpy as np

usage = """Tools implementing methods for merging proton-proton events to heavy ion events.

HITools is licenced under the GNU GPL version 2. Please respect the
MCnet Guideline found at: http://www.montecarlonet.org/GUIDELINES.

Lund University, 2018.
Contact: Johannes Bellm (johannes.bellm@thep.lu.se), Christian Bierlich (christian.bierlich@thep.lu.se)
"""
# From hepmcanalysis. Provide citation here.

def events(genevent):
  '''simple generator that exhausts the read_next_event of a GenEvent object'''
  ev = genevent.read_next_event()
  while ev is not None:
     yield ev
     ev = genevent.read_next_event()

def fromfile(filename):
    proxy = hepmc.ifstream_proxy(filename)
    g = hepmc.IO_GenEvent(proxy.stream())
    for e in events(g):
        yield e

# Here links to external programs: RIVET and the desired generator
# is set up.
def setupFifo(rivetAnalyses):
    ah = rivet.AnalysisHandler()
    os.environ["RIVET_ANALYSIS_PATH"] = "rivet-analyses/"
    for analys in rivetAnalyses:
        ah.addAnalysis(analys)
    outFifo = ".fifo.hepmc"
    if os.path.exists(outFifo):
        os.remove(outFifo)
    os.mkfifo(outFifo)
    return ah


# Routine for running a Rivet analysis.
def analyse(run,ah,output):
    run.init(".fifo.hepmc")
    while run.readEvent():
        run.processEvent()
    run.finalize()
    ah.writeData(output+".yoda")
    return

# Class setting up pipes for input and output of HepMC events.
class Pipe:
    def __init__(self,path,ReadWrite):
        self.path=path
        if ReadWrite == "r":
            self.events=fromfile(path)
        if ReadWrite == "w":
            self.outst=hepmc.ofstream_proxy(path)
    # Get the next event from the pipe.        
    def getNext(self):
        try:
            e=self.events.next()
            return e
        except IOError as (errno, strerror):
            print "I/O error({0}): {1}".format(errno, strerror)
        except ValueError:
            print "Could not convert data to an integer."
        except StopIteration:
            print "Got to end of pipe %s"%self.path   
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
        return None
    def pushNext(self,e):
        e.write(self.outst.stream())
       
# HT=\sum_i p^i_T
def HT(parts):
  res=0
  for p in parts:
    res+=p.momentum().perp()
  return res

# MT=\sum_i m^i_T
def MT(parts):
  MT2=0.
  for p in parts:
    MT2+=p.momentum().m2()
    MT2+=p.momentum().perp2()
  return math.sqrt(MT2)
    
# Implements several schemes to indicate the "hardness" of
# a sub-event, which is supposed to be correlated with 
# pp impact parameter.
def selectHardest(eventList,hardness,retAll = False):
    esort=-1
    # Select the hardest to be the event with most particles:
    if hardness=="NParticles":
      esort=sorted(eventList, key=lambda ev: ev[0].fsParticles().size())
      
    # Select the hardest to be the event with most particles:
    if hardness=="HT":
      esort=sorted(eventList, key=lambda ev: HT(ev[0].fsParticles()))
      
    # Select the hardest to be the event with the largest scale:
    if hardness=="Scale":
      esort=sorted(eventList, key=lambda ev: ev[0].event_scale())
      
    # Select the hardest to be the event with the largest Shat:
    if hardness=="MT":   
      esort=sorted(eventList, key=lambda ev: MT(ev[0].fsParticles()))
    
    if not retAll:
        for rm_event in esort[0:-2]:
            # clear the other events.
            rm_event[0].clear()
    
    if esort==-1:
        print "selectHardest error"
        return NULL
    if retAll:
        return list(reversed(esort))
    return esort[-1][0]
    
    
def getPlusMinus(event,etaMax=7.):
  nplus_out=0.
  nminus_out=0.
  Pplus_out=0.
  Pminus_out=0.
    
  nplus_in=0.
  nminus_in=0.
  Pplus_in=0.
  Pminus_in=0.
  for p in event.fsParticles():   
    eta=p.momentum().eta()
    if abs(eta) < etaMax:
      if eta > 0. :
        nplus_in+=1
        Pplus_in+=(p.momentum().e() + p.momentum().pz())
      else:
        nminus_in+=1
        Pminus_in+=(p.momentum().e() - p.momentum().pz())
    else:
        if eta>0:
            nplus_out+=1.
            Pplus_out+=(p.momentum().e() + p.momentum().pz())
        else:
            nminus_out+=1.
            Pminus_out+=(p.momentum().e() - p.momentum().pz())
  return (Pplus_in,
          Pminus_in,
          nplus_in/(nplus_in+nplus_out),
          Pplus_in/(Pplus_in+Pplus_out),
          nminus_in/(nminus_in+nminus_out),
          Pminus_in/(Pminus_in+Pminus_out))


def correlatedCollisions( absorbtive , others , index , model=1):
    # get the inidices where the others are the same as the absorbitve. 
    if len(others)==0:
        return 1
    sameleft  = np.where(others[:,0]==absorbtive[index][0])[0]
    sameright = np.where(others[:,1]==absorbtive[index][1])[0]
    
    if model==0:
        return 1+len(sameleft)+len(sameright)
    
    elif model==1:
        N=1.
        for k in sameleft:
            if others[k][1] not in absorbtive[:,1]:
                N+=1.
            else:
                N+=1./2.
        for l in sameright:
            if others[l][0] not in absorbtive[:,0]:
                N+=1.
            else:
                N+=1./2.
        # X.5 -> X+rnd()<0.5
        return int(N)+(np.random.random()<(N-int(N))) 

        
    else:
        print "model not implemented."
        return -1
        


    
    
