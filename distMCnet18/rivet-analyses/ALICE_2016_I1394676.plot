# BEGIN PLOT /ALICE_2016_I1394676/d01-x01-y01
Title=$\sqrt{s}=2.76$ TeV/nn, Centrality: 30-40 pct. 
XLabel=$\eta$
YLabel=$\frac{1}{N_{evt}}\frac{dN_{ch}}{d\eta}$
LogY=0
LogX=0
# END PLOT
# BEGIN PLOT /ALICE_2016_I1394676/d01-x01-y02
Title=$\sqrt{s}=2.76$ TeV/nn, Centrality: 40-50 pct. 
XLabel=$\eta$
YLabel=$\frac{1}{N_{evt}}\frac{dN_{ch}}{d\eta}$
LogY=0
LogX=0
# END PLOT
# BEGIN PLOT /ALICE_2016_I1394676/d01-x01-y03
Title=$\sqrt{s}=2.76$ TeV/nn, Centrality: 50-60 pct. 
XLabel=$\eta$
YLabel=$\frac{1}{N_{evt}}\frac{dN_{ch}}{d\eta}$
LogY=0
LogX=0
# END PLOT
# BEGIN PLOT /ALICE_2016_I1394676/d01-x01-y04
Title=$\sqrt{s}=2.76$ TeV/nn, Centrality: 60-70 pct. 
XLabel=$\eta$
YLabel=$\frac{1}{N_{evt}}\frac{dN_{ch}}{d\eta}$
LogY=0
LogX=0
# END PLOT
# BEGIN PLOT /ALICE_2016_I1394676/d01-x01-y05
Title=$\sqrt{s}=2.76$ TeV/nn, Centrality: 70-80 pct. 
XLabel=$\eta$
YLabel=$\frac{1}{N_{evt}}\frac{dN_{ch}}{d\eta}$
LogY=0
LogX=0
# END PLOT
# BEGIN PLOT /ALICE_2016_I1394676/d01-x01-y06
Title=$\sqrt{s}=2.76$ TeV/nn, Centrality: 80-90 pct. 
XLabel=$\eta$
YLabel=$\frac{1}{N_{evt}}\frac{dN_{ch}}{d\eta}$
LogY=0
LogX=0
# END PLOT
