/**
 * @file   AlicePrimary.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Apr 27 17:46:46 2017
 * 
 * @brief  Projection to select primary particles according to the
 * ALICE definition.
 * @copyright GNU Lesser Public License.
 * 
 * @ingroup alice_rivet_primary
 */
#include "AlicePrimary.hh"
#include <Rivet/Particle.hh>
#include <Rivet/Event.hh>
#include <Rivet/Tools/ParticleIdUtils.hh>
#include <Rivet/Tools/ParticleName.hh>
#include <HepMC/GenParticle.h>
#include <HepMC/GenVertex.h>

namespace Rivet
{
    // Compare 
    int Primary::compare(const Projection& p) const
    {
      const Primary* o = dynamic_cast<const Primary*>(&p);
      if (!o) return UNDEFINED;
      if (o->_pdg != _pdg) return UNDEFINED;
      return _cuts == o->_cuts ? EQUIVALENT : UNDEFINED;
    }
    // Project
    void Primary::project(const Event& e)      
    {
      MSG_DEBUG("Projecting out primary particles");
      _theParticles.clear(); // Clear cache
      bool open = _cuts == Cuts::open(); 
      for (auto p : Rivet::particles(e.genEvent())) {
	if (isPrimary(p,_pdg) && (open || _cuts->accept(Particle(p)))) 
	  _theParticles.push_back(Particle(*p));
      }
    }
    // Check if primary
    bool Primary::isPrimary(const HepMC::GenParticle* p,
			    int                       pdg)
    {
      if (mustIgnore(p)) return false;
      MSG_TRACE(p->barcode() << '/' << p->pdg_id() << '/' << p->status()
		<< " is real");

      if (pdg != 0) {
	if (p->pdg_id() != pdg) return false;
	MSG_TRACE(p->barcode() << '/' << p->pdg_id() << '/' << p->status()
		  << " has right PID");

      }
      else if (!isLongLived(p)) 
	return false;
      MSG_TRACE(p->barcode() << '/' << p->pdg_id() << '/' << p->status()
		<< " is long-lived");
      

      // Loop back over ancestors that are _not_ ignored 
      const HepMC::GenParticle* m = p;
      while ((m = ancestor(m,true))) {
	if (isBeam(m)) return true;
	MSG_TRACE("(grand)mother "
		  << m->barcode() << '/' << m->pdg_id() << "/" << m->status()
		  << " not beam ");
	if (isLongLived(m)) return false;
	MSG_TRACE("(grand)mother "
		  << m->barcode() << '/' << m->pdg_id() << "/" << m->status()
		  << " is short-lived");
	if (!hasDecayed(m)) return false;
	MSG_TRACE("(grand)mother "
		  << m->barcode() << '/' << m->pdg_id() << "/" << m->status()
		  << " has decayed");
      }
      MSG_TRACE(p->barcode() << '/' << p->pdg_id() << '/' << p->status()
		<< " is primary");
      return true;
    }
    // Particles with status code defined by the HepMC standard as
    // comment codes, or model specific codes.  Transport codes (>
    // 200) are not ignored.
    bool Primary::mustIgnore(const HepMC::GenParticle* p)
    {
      return p && (p->status()==0 || (p->status()>=11 && p->status()<=200));
    }
    // Check if long-lived
    bool Primary::isLongLived(const HepMC::GenParticle* p)
    {
      int pdg = PID::abspid(p->pdg_id());
      // Check for nuclus 
      if (pdg > 1000000000) return true;
	
      switch (pdg) {
      case Rivet::PID::MUON:
      case Rivet::PID::ELECTRON:
      case Rivet::PID::GAMMA:
      case Rivet::PID::PIPLUS:
      case Rivet::PID::KPLUS:
      case Rivet::PID::K0S:
      case Rivet::PID::K0L:
      case Rivet::PID::PROTON:
      case Rivet::PID::NEUTRON:
      case Rivet::PID::LAMBDA:
      case Rivet::PID::SIGMAMINUS:
      case Rivet::PID::SIGMAPLUS:
      case Rivet::PID::XIMINUS:
      case Rivet::PID::XI0:
      case Rivet::PID::OMEGAMINUS:
      case Rivet::PID::NU_E:
      case Rivet::PID::NU_MU:
      case Rivet::PID::NU_TAU:
	return true;
      }
      return false;
    }
    bool Primary::isBeam(const HepMC::GenParticle* p)
    {
      // Pythia6 uses 3 for initial state
      return p && (p->status() == 3 || p->status() == 4); 
    }
    bool Primary::hasDecayed(const HepMC::GenParticle* p)
    {
      return p && p->status() == 2;
    }
    // Get immediate ancestor 
    const HepMC::GenParticle* Primary::ancestor(const HepMC::GenParticle* p)
    {
      const HepMC::GenVertex* vtx = p->production_vertex();
      if (!vtx) return 0;
      HepMC::GenVertex::particles_in_const_iterator i =
	vtx->particles_in_const_begin();
      if (i == vtx->particles_in_const_end()) return 0;
      return *i;
    }
    // Get immediate ancestor 
    const HepMC::GenParticle* Primary::ancestor(const HepMC::GenParticle* p,
						bool)
    {
      const HepMC::GenParticle* m = p;
      do {
	m = ancestor(m);
      } while (m && mustIgnore(m));
      return m;
    }
}
//
// EOF
//
