// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include<string>
#include<iostream>
#include "Centrality.h"
#include "AlicePrimary.hh"

namespace Rivet {
// ALICE PbPb at 2.76 TeV eta distributions, centra from:
// https://hepdata.net/record/ins1225979
// http://inspirehep.net/record/1225979 
class ALICE_2013_I1225979 : public Analysis {
  public:

    /// Constructor
    ALICE_2013_I1225979()
      : Analysis("ALICE_2013_I1225979")
    {    }


    /// @ame Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {
      ChargedFinalState vzero( ((Cuts::eta > 2.8 && Cuts::eta < 5.1) || (Cuts::eta > -3.7 && Cuts::eta < -1.7)) && Cuts::pT > 0.1*GeV );
    //  ChargedFinalState vzero( Cuts::abseta < 3.84 && Cuts::abseta > 2.09 && Cuts::pT > 0.1*GeV);    
      ChargedFinalState spd(Cuts::abseta < 1. && Cuts::pT > 0.15*GeV);
      Primary alicePrim(Cuts::abseta < 5.5);
      //ChargedFinalState alicePrim(Cuts::abseta < 5.5);
      addProjection(vzero,"VZERO");
      addProjection(spd,"SPD");
      addProjection(alicePrim,"APRIM");
      
      Histo1DPtr hist1 = bookHisto1D("etaCent1",400,0.0,4200.0);
      
      caEta = CentralityHistogram<double>("etaCent1",400,0.0,4200.0,hist1);
      vector<double>centData = {0., 0.05, 0.1, 0.2, 0.3};
      for(size_t i = 0; i < centData.size() - 1; ++i){
	// eta hists starts from table 1, centrality starts at 0
      	caEta.addHistogram(bookHisto1D(1, 1, 1 + i),centData[i],centData[i+1]);
      }
      
     }

    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const double weight = event.weight();
      const Primary& alicePrim = applyProjection<Primary>(event,"APRIM");
      //const ChargedFinalState& alicePrim = applyProjection<ChargedFinalState>(event,"APRIM");
      const ChargedFinalState& vzero = applyProjection<ChargedFinalState>(event,"VZERO");
      const ChargedFinalState& spd = applyProjection<ChargedFinalState>(event,"SPD");

      double sumEt = 0.0;
      int fwdTrig = 0;
      int bwdTrig = 0;
      int cenTrig = 0;
      for(const Particle& p : vzero.particles()) {
 	      sumEt+=p.Et();
	      if (!fwdTrig && p.eta() > 0)
		      fwdTrig = 1;
	      if (!bwdTrig && p.eta() < 0)
		      bwdTrig = 1;
      }
      if(spd.particles().size() > 0) 
	      cenTrig = 1;

      // Use for centrality caclulation if signal both forward and backward
      if (fwdTrig + bwdTrig == 2)	
        caEta.eventFill(weight,sumEt);
      // For multiplicity only two out of three triggers needs to fire
      if (fwdTrig + bwdTrig + cenTrig > 1) {
        for (const Particle& p : alicePrim.particles()){
      	  if(p.abscharge() > 0)	
	    caEta.fill(p.eta(),weight,sumEt);
          }
      }
      
     
    }


    /// Normalise histograms etc., after the run
    void finalize() {
	    caEta.finalize();
    }

    //@}


  private:
	CentralityHistogram<double> caEta;

  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ALICE_2013_I1225979);


}
