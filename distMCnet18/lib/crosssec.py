#! /usr/bin/env/python

from math import *
usage = """Class implementing cross section calculations by Schuler and Sjostrand,
originally published in G.A. Schuler and T. Sjostrand, Phys. Rev. D49 (1994) 2257
. Intended to give a HiWig user an easy option for calculating (semi)-inclusive pp
cross sections used as model input. Please cite the above paper if this is used.
All blame for errors in the implementation rests with the authors of the implementation,
contact info below.

The present implementation is licenced under the GNU GPL version 2. Please respect the
MCnet Guideline found at: http://www.montecarlonet.org/GUIDELINES.

Lund University, 2017.
Contact: Johannes Bellm (johannes.bellm@thep.lu.se), Christian Bierlich (christian.bierlich@thep.lu.se)
"""

class CrossSections:
    def __init__(self):
        # initialize data members with zero values
        self.sigTot = 0.
        self.sigEl = 0.
        self.sigSD = 0.

    def calculate(self, ecm):
      s = ecm*ecm;
      # Total cross section is eq. 4 in the paper
      X = 21.7
      Y = 56.08
      epsilon = 0.0808
      eta = 0.4525
      self.sigTot = X*pow(s,epsilon) + Y*pow(s,-eta)
      
      # Elastic cross section is sigTot^2/(16pi B_el)
      # where B_el is the elastic slope given by eq 11 in the paper
      # C1 = 2*c0/pow(s0,epsilon)
      C1 = 4.005207
      # C2 = 2*c1
      C2 = 4.2
      # In eq ba = bb = bp = 2.3
      bp = 2.3
      bEl = 4*bp + C1*pow(s,epsilon) - C2
      # coeff = 1/(16pi)/0.389 (converts mb vs GeV^2) 
      coeff = 0.0511423
      self.sigEl = coeff*self.sigTot*self.sigTot/bEl

      # Single diffraction is not currently used in Hiwig, but 
      # implemented here anyway for future usage and cross check. 
      # From eq. 24 it is:
      # g_3p/(16pi)*beta^2_ap(0)\beta_bp(0)I_ax
      # beta_ap(0)*beta_bp(0) = X (cf. eq. 4)
      betaprod = X*4.658
      # coeff = g3p/(16pi)/0.389
      coeff = 0.0336
      # I_ax is given by eq. 25
      # alpha' = 1/s0
      ap = 0.25
      # mmin = 2*pion mass
      mmin = 0.28
      # ma = proton mass
      ma = 0.94
      m2min2 = (ma + mmin)*(ma + mmin)
      # End of spectrum from article text
      m2res = 2.
      # Enhancement factor for low mass ressonances, eq. 22
      cres = 2.
      # B_ax from eq. 26
      bax = -0.47 + 150./s
      # m^2max_ax from eq. 26
      mmax2 = 0.213*s
      # Beginning the long expression
      firstTerm = 1./2./ap*log((2*bp + 2*ap*log(s/m2min2))/(2*bp + 2*ap*log(s/mmax2)) )
      secondTerm = cres*log(1 + m2res*m2res/m2min2)/(2*bp + 2*ap*log(s/m2res/sqrt(m2min2)) + bax)
      iax = firstTerm + secondTerm
      self.sigSD = max(0.,coeff*betaprod*iax)

      return self.sigTot, self.sigEl, self.sigSD


        
