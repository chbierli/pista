// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/WFinder.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "fastjet/tools/Filter.hh"
#include "fastjet/tools/Pruner.hh"

namespace Rivet {

  


  class HI_JET : public Analysis {
  public:

    /// @name Constructors etc.
    //@{

    /// Constructor
    HI_JET()
      : Analysis("HI_JET")
    {    }

    //@}


  public:

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {
      FinalState fs(Cuts::abseta < 2.5);
      declare(fs, "FS");

      ZFinder zfinder(fs, Cuts::abseta < 2.5 && Cuts::pT > 30*GeV, PID::ELECTRON, 80*GeV, 100*GeV,
                      0.2, ZFinder::CLUSTERNODECAY, ZFinder::TRACK);

      declare(zfinder, "ZFinder");

      // Z+jet jet collections
      declare(FastJets(zfinder.remainingFinalState(), FastJets::ANTIKT, 0.3), "JetsAK3");
      declare(FastJets(zfinder.remainingFinalState(), FastJets::ANTIKT, 0.5), "JetsAK5");
      declare(FastJets(zfinder.remainingFinalState(), FastJets::ANTIKT, 0.7), "JetsAK7");
      declare(FastJets(zfinder.remainingFinalState(), FastJets::ANTIKT, 0.9), "JetsAK9");

      jetFinders = {"JetsAK3", "JetsAK5", "JetsAK7", "JetsAK9"};
      for (size_t i = 0; i < jetFinders.size(); ++i) {
        string s = jetFinders[i];
	h_rat.push_back(bookHisto1D(s + "rat",20,0.,2.));
	h_ratAll.push_back(bookHisto1D(s + "ratAll",20,0.,2.));
	h_zpT.push_back(bookHisto1D(s + "zpT",100,0,1000));
	h_jetpT.push_back(bookHisto1D(s + "jetpT",100,0,1000));

      }
    }

    bool isBackToBack_zj(const ZFinder& zf, const fastjet::PseudoJet& psjet) {
      const FourMomentum& z = zf.bosons()[0].momentum();
      const FourMomentum jmom(psjet.e(), psjet.px(), psjet.py(), psjet.pz());
      return (deltaPhi(z, jmom) > 7.*M_PI/8. );
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const double weight = event.weight();

      // Get the Z
      const ZFinder& zfinder = apply<ZFinder>(event, "ZFinder");
      if (zfinder.bosons().size() != 1) vetoEvent;
      Particle z = zfinder.bosons()[0];
      Particle l1 = zfinder.constituents()[0];
      Particle l2 = zfinder.constituents()[1];
      // Require a high-pT Z (and constituents)
      if (l1.pT() < 10*GeV || l2.pT() < 10*GeV || z.pT() < 60*GeV) vetoEvent;
      // Get the  jets
      for (size_t i = 0; i < jetFinders.size(); ++i ) {
        const PseudoJets& psjets = apply<FastJets>(event, 
	  jetFinders[i]).pseudoJetsByPt(30.0*GeV);
        if (!psjets.empty()) {
        // Get the leading jet and make sure it's back-to-back with the Z
        const fastjet::PseudoJet& j0 = psjets[0];
	if (isBackToBack_zj(zfinder, j0)) {
            h_jetpT[i]->fill(j0.perp(),weight);
	    h_zpT[i]->fill(z.pT(),weight);
	    h_rat[i]->fill(j0.perp()/z.pT(),weight);
	  }
	for (size_t j = 0; j < psjets.size(); ++j) {
	  const fastjet::PseudoJet& jj = psjets[j];
	  if (jj.perp() > 60*GeV && jj.eta() < 1.6 && 
	    isBackToBack_zj(zfinder, jj)) {
	    h_ratAll[i]->fill(jj.perp()/z.pT(),weight);
	  }
	}
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {
       for(size_t i = 0; i < jetFinders.size(); ++i) {
         h_ratAll[i]->scaleW(1./sumOfWeights());
         h_rat[i]->scaleW(1./sumOfWeights());
         h_zpT[i]->scaleW(1./sumOfWeights());
         h_jetpT[i]->scaleW(1./sumOfWeights());
       }
    }


    //@}


  private:
  vector<string> jetFinders;
  vector<Histo1DPtr> h_rat;
  vector<Histo1DPtr> h_ratAll;
  vector<Histo1DPtr> h_zpT;
  vector<Histo1DPtr> h_jetpT;

  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(HI_JET);

}
