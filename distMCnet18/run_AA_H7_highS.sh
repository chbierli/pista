#!/bin/sh
#SBATCH -t 09:00:00
# specify project
#BATCH -A hep2016-1-5
# specify storage
#SBATCH -p hep
HOME=$PWD
source /home/jbellm/opt/bin/activate
#export RIVET_ANALYSIS_PATH=/lunarc/nobackup/users/jbellm/hiwig/hg/rivet-analyses
export RIVET_ANALYSIS_PATH=/lunarc/nobackup/users/jbellm/hiwig/dist/dist/rivet-analyses
cd input/herwig
rm -f *.fifo
mkfifo absAA_highS.fifo
mkfifo difAA1_highS.fifo
mkfifo difAA2_highS.fifo

Herwig read absAA_highS.in
Herwig read difAA1_highS.in
Herwig read difAA2_highS.in


Herwig run absAA_highS.run  -s$RANDOM -N1000000000 &> H7_absAA_highS.log &
RUNNING_PID1=$!
Herwig run difAA1_highS.run -s$RANDOM -N1000000000 &> H7_difAA1_highS.log &
RUNNING_PID2=$!
Herwig run difAA2_highS.run -s$RANDOM -N1000000000 &> H7_difAA2_highS.log &
RUNNING_PID3=$!



cd $HOME/input
rm -f *fifo
ln -s herwig/absAA_highS.fifo abs.fifo
ln -s herwig/difAA1_highS.fifo dif1.fifo
ln -s herwig/difAA2_highS.fifo dif2.fifo
cd $HOME

export PYTHONPATH=/home/jbellm/.local/lib/python2.7/site-packages/:$PYTHONPATH
python pista.py 3

kill -9 $RUNNING_PID1
kill -9 $RUNNING_PID2
kill -9 $RUNNING_PID3

(cd input;rm   *pyc *fifo *run *log *out)
rm lib/*pyc
