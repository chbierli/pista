// pythia.cc is a custom PYTHIA main program written for the purpose of generating
// sub-events to stack in heavy ion collisions using HIWIG, based on the PYTHIA main42.cc
// example program distributed with PYTHIA.
// The PYTHIA program is copyright (C) 2017 Torbjorn Sjostrand.
// pythia.cc is not supported by the PYTHIA development team, questions
// should instead be directed to:
// Johannes Bellm (johannes.bellm@thep.lu.se) or 
// Christian Bierlich (christian.bierlich@thep.lu.se)

#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"

using namespace Pythia8;

int main(int argc, char* argv[]) {

  // Check that correct number of command-line arguments
  if (argc != 3 && argc != 4) {
    cerr << " Unexpected number of command-line arguments. \n You are"
         << " expected to provide one input and one output file name. \n"
         << " Program stopped! " << endl;
    return 1;
  }

  // Check that the provided input name corresponds to an existing file.
  ifstream is(argv[1]);
  if (!is) {
    cerr << " Command-line file " << argv[1] << " was not found. \n"
         << " Program stopped! " << endl;
    return 1;
  }

  // Confirm that external files will be used for input and output.
  cout << "\n >>> PYTHIA settings will be read from file " << argv[1]
       << " <<< \n >>> HepMC events will be written to file "
       << argv[2] << " <<< \n" << endl;


  // Interface for conversion from Pythia8::Event to HepMC event.
  HepMC::Pythia8ToHepMC ToHepMC;

  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_io(argv[2], std::ios::out);

  // Generator.
  Pythia pythia;

  // Add possibility to select only diffractive process
  // from left or right.
  pythia.settings.addFlag("Main:remove103",false);
  pythia.settings.addFlag("Main:remove104",false);
  // Read in commands from external file.
  pythia.readFile(argv[1]);

  // Check if we have set a seed
  if (argc == 4) {
     string seed = string(argv[3]);
     pythia.readString("Random:setSeed = on");
     pythia.readString("Random:seed = "+seed);
  }
  // Extract settings to be used in the main program.
  int nAbort = pythia.mode("Main:timesAllowErrors");

  // Diffractive process to be removed
  bool rem103      = pythia.flag("Main:remove103");
  bool rem104      = pythia.flag("Main:remove104");
  if (rem103 && rem104) {
    cout << "You have selected to remove diffractive "
	    "events from both sides. Terminating!" << endl;
    return 1;
  }
  int rem = 104;
  if (rem103) rem = 103;
  // Initialization.
  pythia.init();

  // Begin event loop.
  int iAbort = 0;
  while (true) {

    // Generate event.
    // if it is a single diffractive event, we only want from one side
    do{  
   	 if (!pythia.next()) {
   	      // First few failures write off as "acceptable" errors, then quit.
	      if (++iAbort < nAbort) continue;
	      cout << " Event generation aborted prematurely, owing to error!\n";
	      break;
	    }

    } while(pythia.info.code() == rem);

    // Construct new empty HepMC event and fill it.
    // Units will be as chosen for HepMC build, but can be changed
    // by arguments, e.g. GenEvt( HepMC::Units::GEV, HepMC::Units::MM)
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
    ToHepMC.fill_next_event( pythia, hepmcevt );

    // write information on the number of mpis
    hepmcevt->set_mpi(pythia.info.nMPI()-1);

    // Write the HepMC event to file. Done with it.
    ascii_io << hepmcevt;
    delete hepmcevt;

  // End of event loop. Statistics.
  }
  pythia.stat();

  // Done.
  return 0;
}
