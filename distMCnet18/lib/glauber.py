#! /usr/bin/env/python

usage = """Classes implementing methods for simple Glauber calculation. Depends on impactMC tools.
Defines a base class for Glauber calculations, responsible for setting up nuclear geometry using
a Woods Saxon potential in the GLISSANDO parametrization. From this base class, one can derive 
specific Glauber models, like black disks, grey disks or similar.

The Glauber implementation  is licenced under the GNU GPL version 2. Please respect the
MCnet Guideline found at: http://www.montecarlonet.org/GUIDELINES.

Lund University, 2017.
Contact: Johannes Bellm (johannes.bellm@thep.lu.se), Christian Bierlich (christian.bierlich@thep.lu.se)
"""

import math
from random import random
import numpy as np
# Base class for Glauber models
from impactMC import *
class GlauberBase:
    def __init__(self, proj, targ):
        self.proj = int(proj)
        self.targ = int(targ)
        self.Zp = 1
        self.Ap = 1
        self.Zt = 1
        self.At = 1
        if self.proj != 2212:
            self.Zp = int(proj[3:6])
            self.Ap = int(proj[6:9])

        if self.targ != 2212:
            self.Zt = int(targ[3:6])
            self.At = int(targ[6:9])
        # Glissando parametrization
        self.rp = 1.1*math.pow(float(self.Ap),1.0/3.0) - 0.656*math.pow(float(self.Ap),-1.0/3.0)
        self.rt = 1.1*math.pow(float(self.At),1.0/3.0) - 0.656*math.pow(float(self.At),-1.0/3.0)
        self.aWs = 0.459
        self.hc = 0.9

    # Setup a hydrogen nucleus
    def setupProton(self):
        pData = {'bx' : 0., 'by' : 0., 'bz' : 0.}
        nucleons = [Particle(pData)]
        return nucleons

    # Setup a nucleus, return a N
    def setupNucleus(self, A, R):
        nucleons = []
        n = Particle()
        while len(nucleons) < A:
            overlap = True
            while overlap:
                wp = 0.
                u = 1.
                x = 0.
                y = 0.
                z = 0.
                while wp < u:
                    x = 2.*R*(2.*random() - 1.)
                    y = 2.*R*(2.*random() - 1.)
                    z = 2.*R*(2.*random() - 1.)
                    r = math.sqrt(x*x + y*y + z*z)
                    wp = 1./(1. + math.exp((r - R)/self.aWs))
                    u = random()
                pData = {'bx' : x, 'by' : y, 'bz' : z}
                n = Particle(pData)
                overlap = False
                for nu in nucleons:
                    if nu.dist(n) < self.hc:
                        overlap = True
                        break
            nucleons.append(n)
        return nucleons


# Sample a point in b-space
    def impactFlat(self, bMin, bMax):
        if bMin < 0 or bMax <=0 or bMin > bMax:
            raise Exception("b limit inconsistency")
        b = math.sqrt(bMin*bMin+(bMax*bMax-bMin*bMin)*random());
        w = math.pi/(bMax*bMax-bMin*bMin);
        return b, 2*math.pi*random(), w

    def impactExp(self, width):
        r=random()
        b = -2.0*math.log(r)*width
        phi = 2.0*math.pi*random()
        w = 0.04*2.*math.pi*width*b/r
        return b, phi, w

    def impactGauss(self,width):
        r = random()
        b   = np.sqrt(-math.log(r)*2.*width*width)
        phi = 2.0*math.pi*random()
        w = 1./r
        return b, phi, w
 


class BlackDisk(GlauberBase):
    # Set up a black disk, sigma is nn cross section in units of mb.
    def __init__(self,proj,targ,sigma):
        GlauberBase.__init__(self,proj,targ)
        self.sigma = sigma

    def next(self,bIn):
        # target sits in 0,0 
        t = []
        if self.targ == 2212:
            t = self.setupProton()
        else:
            t = self.setupNucleus(self.At,self.rt)
        p = []
        if self.proj == 2212:
            p = self.setupProton()
        else:
            p = self.setupNucleus(self.Ap,self.rp)
        self.b, self.phi, self.w = self.impactGauss(bIn)
        scols = []
        parts = []
        # add particles to output
        ind = 0
        for pn in p:
            pn.shift(self.b,self.phi)
            pn['index'] = ind
            pn['pid'] = 2212
            pn['px'] = 0.
            pn['py'] = 0.
            pn['pz'] = 0.
            pn['E'] = 0.
            ind += 1
            parts.append(pn)
        for tn in t:
            tn['index'] = ind
            tn['pid'] = 2212
            tn['px'] = 0.
            tn['py'] = 0.
            tn['pz'] = 0.
            tn['E'] = 0.
            ind += 1
            parts.append(tn)

        for i,pn in enumerate(p):
            for j,tn in enumerate(t):
                dist = tn.dPerp(pn)
                if dist < math.sqrt(self.sigma/10./math.pi):
                    sd = {'i1' : i, 'i2' : len(p)+j, 'type' : 1, 'scale' : 1./dist}
                    scols.append(SubCollision(sd))
        return parts,scols

class GribovDisk(GlauberBase):
    # Set up a Gribov disk, sigma is nn cross section in units of mb.
    def __init__(self,proj,targ,mu,sigma):
        GlauberBase.__init__(self,proj,targ)
        self.sigma = sigma
        self.mu = mu

    def next(self,bIn):
        # target sits in 0,0 
        t = []
        if self.targ == 2212:
            t = self.setupProton()
        else:
            t = self.setupNucleus(self.At,self.rt)
        p = []
        if self.proj == 2212:
            p = self.setupProton()
        else:
            p = self.setupNucleus(self.Ap,self.rp)
        self.b, self.phi, self.w = self.impactGauss(bIn)
        scols = []
        parts = []
        # add particles to output
        ind = 0
        for pn in p:
            pn.shift(self.b,self.phi)
            pn['index'] = ind
            pn['pid'] = 2212
            pn['px'] = 0.
            pn['py'] = 0.
            pn['pz'] = 0.
            pn['E'] = 0.
            ind += 1
            parts.append(pn)
        for tn in t:
            tn['index'] = ind
            tn['pid'] = 2212
            tn['px'] = 0.
            tn['py'] = 0.
            tn['pz'] = 0.
            tn['E'] = 0.
            ind += 1
            parts.append(tn)
        z = math.sqrt(-2*np.log(random()))*np.cos(2*np.pi*random());
	z = self.sigma*z + self.mu;
	s = np.exp(z);	 
        for i,pn in enumerate(p):
            for j,tn in enumerate(t):
                dist = tn.dPerp(pn)
                if dist < math.sqrt(s/10./math.pi):
                    sd = {'i1' : i, 'i2' : len(p)+j, 'type' : 1, 'scale' : 1./dist}
                    scols.append(SubCollision(sd))
        return parts,scols


