// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include<string>
#include<iostream>
#include "Centrality.h"
namespace Rivet {

  class ATLAS_2015_I1386475 : public Analysis {
  public:

    /// Constructor
    ATLAS_2015_I1386475()
      : Analysis("ATLAS_2015_I1386475")
    {    }


    /// @ame Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      ChargedFinalState cfs(Cuts::eta > -2.7 && Cuts::eta < 2.7 && Cuts::pT > 0.1*GeV);
      FinalState cfsForward(Cuts::eta < -3.2 && Cuts::eta > -4.9 && Cuts::pT > 0.1*GeV);
      FinalState minBiasB(Cuts::eta > 2.09 && Cuts::eta < 3.84 && Cuts::pT > 0.1*GeV);
      FinalState minBiasF(Cuts::eta < -2.09 && Cuts::eta > -3.84 && Cuts::pT > 0.1*GeV);
      addProjection(cfs,"CFS");
      addProjection(cfsForward,"CFSF");
      addProjection(minBiasB,"MBB");
      addProjection(minBiasF,"MBF");
      
      Histo1DPtr hist = bookHisto1D("etCent",400,0.0,200.0);
      caEt = CentralityHistogram<double>("eTCent",400,0.0,200.0,hist);
      
      vector<double>centData = {0.00, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.6, 0.9};
      for(size_t i = 0; i < centData.size() - 1; ++i)
      	caEt.addHistogram(bookHisto1D(2,1,8-i),centData[i],centData[i+1]);
      
      
     }
      


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const double weight = event.weight();
      const ChargedFinalState& parts = applyProjection<ChargedFinalState>(event,"CFS");
      const FinalState& forw = applyProjection<FinalState>(event,"CFSF");
      const FinalState& mbf = applyProjection<FinalState>(event,"MBF");
      const FinalState& mbb = applyProjection<FinalState>(event,"MBB");

      if(mbb.particles().size() < 1 || mbf.particles().size() < 1)
	      vetoEvent;

      double sumEt = 0.0;
      for(const Particle& p : forw.particles())
	     sumEt+=p.Et();
      caEt.eventFill(weight,sumEt);
      for (const Particle& p : parts.particles()){
      		caEt.fill(p.eta(),weight,sumEt);
      }
     
    }


    /// Normalise histograms etc., after the run
    void finalize() {
	    caEt.finalize();

    }

    //@}


  private:
	CentralityHistogram<double> caEt;

  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAS_2015_I1386475);


}
