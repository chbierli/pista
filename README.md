Pista quickstart
-----------------

Pista is set up to run on several example systems without any need
for interference from the user -- essentially reproducing the 
figures in arXiv:cccccccc.
To that end, Pista is shipped with:

1) Herwig 7 and Pythia 8 run cards and programs, to produce sub-events.
2) Several sample Rivet analyses for pA and AA.
3) Scripts to run the input generators as input to Pista.
4) Sample JEWEL+Pythia events (for a realistic analysis a larger
sample can be downloaded from ...)

Prerequisites:
You must have the event generator installed, which should deliver pp 
sub-events, preferably a new version. Pista has been tested with 
Pythia v. 8.235 and Herwig 7 v. xxxx
You must have Rivet installed to use the built-in data comparison tools.

To produce multiplicity figures in PbPb @ 2.76 TeV using Pythia as input,
do the following.

1) Compile the rivet analyses:
$> cd rivet-analyses
$> make

2) Setup input fifo
$> cd ../input/pythia
edit the Makefile to have it point to your Pythia installation directory.
$> make pythia
$> source runAA.sh &

3) Run Pista
$> cd ../..
$> python pista.py 2

