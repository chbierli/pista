%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: -80 -63 501 16 
%%HiResBoundingBox: -79.748 -62.86934 500.1014 15.26561 
%%Creator: MetaPost 2.000
%%CreationDate: 2018.04.05:1048
%%Pages: 1
%%DocumentResources: procset mpost-minimal
%%+ font CMR10
%%DocumentSuppliedResources: procset mpost-minimal
%%DocumentNeededResources: font CMR10
%%IncludeResource: font CMR10
%%EndComments
%%BeginProlog
%%BeginResource: procset mpost-minimal
/bd{bind def}bind def/fshow {exch findfont exch scalefont setfont show}bd
/fcp{findfont dup length dict begin{1 index/FID ne{def}{pop pop}ifelse}forall}bd
/fmc{FontMatrix dup length array copy dup dup}bd/fmd{/FontMatrix exch def}bd
/Amul{4 -1 roll exch mul 1000 div}bd/ExtendFont{fmc 0 get Amul 0 exch put fmd}bd
/ScaleFont{dup fmc 0 get Amul 0 exch put dup dup 3 get Amul 3 exch put fmd}bd
/SlantFont{fmc 2 get dup 0 eq{pop 1}if Amul FontMatrix 0 get mul 2 exch put fmd}bd
%%EndResource
%%EndProlog
%%BeginSetup
 /cmr10 /CMR10 def
%%EndSetup
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinejoin 10 setmiterlimit
newpath -79.498 -6.45926 moveto
79.498 -6.45926 lineto
79.498 6.45926 lineto
-79.498 6.45926 lineto
 closepath stroke
newpath -70.33095 -41.31496 moveto
70.33095 -41.31496 lineto
70.33095 -26.45926 lineto
-70.33095 -26.45926 lineto
 closepath stroke
newpath 165.03236 -62.61934 moveto
239.73378 -33.88712 lineto
165.03236 -5.15489 lineto
90.33095 -33.88712 lineto
 closepath stroke
newpath 259.73378 -41.25961 moveto
322.82529 -41.25961 lineto
322.82529 -26.51462 lineto
259.73378 -26.51462 lineto
 closepath stroke
newpath 416.24622 -33.88712 moveto
416.24622 -29.29945 412.37776 -24.90024 405.49344 -21.65652 curveto
398.6091 -18.41281 389.2724 -16.59009 379.53575 -16.59009 curveto
369.7991 -16.59009 360.4624 -18.41281 353.57806 -21.65652 curveto
346.69374 -24.90024 342.82529 -29.29945 342.82529 -33.88712 curveto
342.82529 -38.47478 346.69374 -42.874 353.57806 -46.1177 curveto
360.4624 -49.36142 369.7991 -51.18414 379.53575 -51.18414 curveto
389.2724 -51.18414 398.6091 -49.36142 405.49344 -46.1177 curveto
412.37776 -42.874 416.24622 -38.47478 416.24622 -33.88712 curveto closepath stroke
 1 setlinecap
newpath 79.498 0 moveto
165.03236 0 lineto
165.03236 -5.15489 lineto stroke
newpath 166.6893 -1.15485 moveto
165.03236 -5.15489 lineto
163.37543 -1.15485 lineto
 closepath
gsave fill grestore stroke
newpath 70.33095 -33.88712 moveto
90.33095 -33.88712 lineto stroke
newpath 86.33069 -32.23009 moveto
90.33095 -33.88712 lineto
86.33069 -35.54414 lineto
 closepath
gsave fill grestore stroke
newpath 239.73378 -33.88712 moveto
259.73378 -33.88712 lineto stroke
newpath 255.73352 -32.23009 moveto
259.73378 -33.88712 lineto
255.73352 -35.54414 lineto
 closepath
gsave fill grestore stroke
newpath 322.82529 -33.88712 moveto
342.82529 -33.88712 lineto stroke
newpath 338.82503 -32.23009 moveto
342.82529 -33.88712 lineto
338.82503 -35.54414 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 379.53575 -16.59009 moveto
379.53575 3.40991 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 377.87872 -0.59035 moveto
379.53575 3.40991 lineto
381.19278 -0.59035 lineto
 closepath
gsave fill grestore stroke
newpath 416.2462 -33.88712 moveto
436.2462 -33.88712 lineto stroke
newpath 432.24594 -32.23009 moveto
436.2462 -33.88712 lineto
432.24594 -35.54414 lineto
 closepath
gsave fill grestore stroke
-76.498 -3.45926 moveto
(Glaub) cmr10 9.96265 fshow
-49.585 -3.45926 moveto
(er) cmr10 9.96265 fshow
-37.9342 -3.45926 moveto
(calculation:) cmr10 9.96265 fshow
16.30699 -3.45926 moveto
(User) cmr10 9.96265 fshow
39.3594 -3.45926 moveto
(selection) cmr10 9.96265 fshow
-67.33095 -36.37778 moveto
(Input) cmr10 9.96265 fshow
-39.93365 -36.37778 moveto
(MC:) cmr10 9.96265 fshow
-17.51765 -36.37778 moveto
(Herwig,) cmr10 9.96265 fshow
19.31645 -36.37778 moveto
(Pythia) cmr10 9.96265 fshow
51.83356 -36.37778 moveto
(etc.) cmr10 9.96265 fshow
111.08182 -36.37776 moveto
(Decide) cmr10 9.96265 fshow
143.59882 -36.37776 moveto
(sub-ev) cmr10 9.96265 fshow
171.32822 -36.37776 moveto
(en) cmr10 9.96265 fshow
181.01411 -36.37776 moveto
(t) cmr10 9.96265 fshow
188.20932 -36.37776 moveto
(t) cmr10 9.96265 fshow
191.80692 -36.37776 moveto
(yp) cmr10 9.96265 fshow
202.87662 -36.37776 moveto
(e\(s\)) cmr10 9.96265 fshow
262.73378 -36.32242 moveto
(Merge) cmr10 9.96265 fshow
292.92618 -36.32242 moveto
(ev) cmr10 9.96265 fshow
302.33528 -36.32242 moveto
(en) cmr10 9.96265 fshow
312.02118 -36.32242 moveto
(ts) cmr10 9.96265 fshow
346.3269 -36.32242 moveto
(HepMC) cmr10 9.96265 fshow
383.41011 -36.32242 moveto
(output) cmr10 9.96265 fshow
317.4213 8.3471 moveto
(Stand) cmr10 9.96265 fshow
346.2023 8.3471 moveto
(alone) cmr10 9.96265 fshow
372.2159 8.3471 moveto
(Python) cmr10 9.96265 fshow
407.5004 8.3471 moveto
(analysis) cmr10 9.96265 fshow
439.2462 -36.37778 moveto
(Riv) cmr10 9.96265 fshow
454.3286 -36.37778 moveto
(et) cmr10 9.96265 fshow
465.9516 -36.37778 moveto
(analysis) cmr10 9.96265 fshow
showpage
%%EOF
