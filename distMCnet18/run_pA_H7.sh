#!/bin/sh
HOME=$PWD
export RIVET_ANALYSIS_PATH=/home/student/pista/distMCnet18/rivet-analyses
cd input/herwig
rm -f *.fifo
mkfifo abspA.fifo
mkfifo difpA.fifo
Herwig read abspA.in
Herwig read difpA.in

Herwig run abspA.run  -s$RANDOM -N1000000000 &> H7_abspA.log &
RUNNING_PID1=$!
Herwig run difpA.run -s$RANDOM -N1000000000 &> H7_difpA.log &
RUNNING_PID2=$!

cd $HOME/input
ln -s herwig/abspA.fifo abs.fifo
ln -s herwig/difpA.fifo dif1.fifo

cd $HOME

python pista.py 1

kill -9 $RUNNING_PID1
kill -9 $RUNNING_PID2
(cd input;rm   *pyc *fifo *run *log *out)
rm lib/*pyc
