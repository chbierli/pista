# BEGIN PLOT /ALICE_2013_I1225979/d01-x01-y01
Title=$\sqrt{s}=2.76$ TeV/nn, Centrality: 0-5 pct. 
XLabel=$\eta$
YLabel=$\frac{1}{N_{evt}}\frac{dN_{ch}}{d\eta}$
LogY=0
LogX=0
# END PLOT
# BEGIN PLOT /ALICE_2013_I1225979/d01-x01-y02
Title=$\sqrt{s}=2.76$ TeV/nn, Centrality: 5-10 pct. 
XLabel=$\eta$
YLabel=$\frac{1}{N_{evt}}\frac{dN_{ch}}{d\eta}$
LogY=0
LogX=0
# END PLOT
# BEGIN PLOT /ALICE_2013_I1225979/d01-x01-y03
Title=$\sqrt{s}=2.76$ TeV/nn, Centrality: 10-20 pct. 
XLabel=$\eta$
YLabel=$\frac{1}{N_{evt}}\frac{dN_{ch}}{d\eta}$
LogY=0
LogX=0
# END PLOT
# BEGIN PLOT /ALICE_2013_I1225979/d01-x01-y04
Title=$\sqrt{s}=2.76$ TeV/nn, Centrality: 20-30 pct. 
XLabel=$\eta$
YLabel=$\frac{1}{N_{evt}}\frac{dN_{ch}}{d\eta}$
LogY=0
LogX=0
# END PLOT
