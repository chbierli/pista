#include<string>
#include<iostream>

namespace Rivet {

class CInterval {
public:

CInterval(double l, double h, Histo1DPtr histo)  : low(l), high(h), 
  bookedHistoPtr(histo) { }
	
~CInterval(){ }
  	
bool isIn(double c) const {
  if (c > low && c <= high )
    return true;
  return false;
}

double getLow() const {
  return low;
}	      
	
double getHigh() const {
  return high;
}

double getCenter() const {
  return (low+high)/2.0;
}

double getIntegral() const {
  return bookedHistoPtr->integral();
}

Histo1DPtr histo() const {
  return bookedHistoPtr;
}

private:

double low, high;

Histo1DPtr bookedHistoPtr;
	      	
};
  
ostream& operator << (ostream& aStream, const CInterval aInterval) {
  aStream << aInterval.getLow() << " " << aInterval.getHigh();
  return aStream;	
}

template<class T>
class EstimatorBin {
public:

EstimatorBin(T minIn, T maxIn) : sow(0.0), sow2(0.0), esow(0.0), esow2(0.0), 
  min(minIn), max(maxIn) { }

EstimatorBin(T& x) : sow(0.0), sow2(0.0), esow(0.0), esow2(0.0), min(x),
  max(0.0) { } 

~EstimatorBin(){ }

void fill(double w) const {
  sow+=w;
  sow2+=w*w;
}
	
void eventFill(double w) const {
  esow+=w;
  esow2+=w*w;
}

T getMin() const {
  return min;	
}

T getMax() const {
  return max;
}

double getSow() const {
  return sow;
}

double getSow2() const {
  return sow2;
}

double getEsow() const {
  return esow;
}

private:
  mutable double sow, sow2, esow, esow2;
  T min, max;
};
  
template<typename T>
bool operator < (const EstimatorBin<T>& l, const EstimatorBin<T>& r) {
  return (l.getMin() < r.getMin());
}

template<typename T>
bool operator < (const T& l, const EstimatorBin<T>& r) {
  return (l < r.getMin());
}


template<typename T>
bool operator < (const EstimatorBin<T>& l, const T& r) {
  return (l.getMin() < r);
}
  
template<class T>
class CentralityHistogram {
        
public:
CentralityHistogram() { }

CentralityHistogram(string nameIn, int nbinsIn, T minIn, T maxIn, 
  Histo1DPtr h) : name(nameIn), nBins(nbinsIn), min(minIn), max(maxIn),
  hist(h), constructed(false) { };

~CentralityHistogram() { }
	
void addHistogram(Histo1DPtr hptr, double cmin, double cmax) {
  if(!constructed){
    constructed = true;
    T delta = (max - min)/((double) nBins);
    for(T a = min; a <= max; a+=delta)
      EstHist[EstimatorBin<T>(a,a+delta)] = Histo1D(*hptr);
  }
  cHistos.push_back(CInterval(cmin, cmax, hptr));	
}
 	
void fill(double val, double weight, T est) {
  typename map<EstimatorBin<T>, Histo1D>::iterator eh = 
    EstHist.lower_bound(est);
  if(eh != EstHist.end()) eh->second.fill(val,weight);	
}

void eventFill(double weight, T est) {
  hist->fill(est,weight);
  typename map<EstimatorBin<T>, Histo1D>::iterator eh = 
    EstHist.lower_bound(est);
  if(eh != EstHist.end()){
    eh->first.fill(weight);
    eh->first.eventFill(weight);	
  }	
}
	
vector<double> integralOfHistograms() {
  vector<double> ret;
  for(vector<CInterval>::iterator cItr = cHistos.begin(); 
    cItr != cHistos.end(); ++cItr){
      ret.push_back(cItr->getIntegral());
  }
  return ret;	
}	

void finalize(){
  double integ = integral(EstHist.rbegin(), EstHist.rend()).first;
  for(vector<CInterval>::iterator cItr = cHistos.begin(); 
    cItr != cHistos.end(); ++cItr){
    double sowSum = 0.0;	
    for(auto itr = EstHist.rbegin(); itr != EstHist.rend(); ++itr){
      double sow = integral(EstHist.rbegin(),itr).first;
      double frac = sow/integ;		
      if( cItr->isIn(frac)){
        sowSum += itr->first.getEsow();	
	*(cItr->histo())+=itr->second;
      }
    }
    hist->normalize();
    cItr->histo()->scaleW(( sowSum == 0 ? 1.0 : 1.0/sowSum));
  }		
}

void scaleAll(vector<double> scales) {
  if(scales.size() != cHistos.size())
    cout << "ERROR! Number of hists and scales are not equal!" << endl;
  for(int i = 0, N = scales.size(); i < N; ++i)
    cHistos[i].histo()->scaleW(scales[i]);
}

void profile(Profile1DPtr h, double nbins){		
  for(vector<CInterval>::iterator cItr = cHistos.begin();
    cItr != cHistos.end(); ++cItr){
    double avg = cItr->histo()->integral()/nbins;
    h->fill(cItr->getCenter(),avg);
  }
}

pair<double,double> integral(typename map<EstimatorBin<T>, Histo1D>::reverse_iterator lo, 
  typename map<EstimatorBin<T>, Histo1D>::reverse_iterator hi){
  double sow = 0.0;
  double sow2 = 0.0;
  for(auto eItr = lo; eItr != hi; ++eItr){
    sow += eItr->first.getSow();
    sow2 += eItr->first.getSow2();
  }
  return make_pair(sow,sow2);
}

private:
vector<CInterval> cHistos;
map<EstimatorBin<T>, Histo1D> EstHist;
string name;
int nBins;
T min, max;
Histo1DPtr hist;
bool constructed;  
};

}
