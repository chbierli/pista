/**
 * @file   AlicePrimary.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Apr 27 17:09:04 2017
 * 
 * @brief Projection to select primary particles according to the
 * ALICE definition.
 * 
 * @copyright GNU Lesser Public License.
 *
 * @ingroup alice_rivet_primary
 */
#ifndef ALICEPRIMARY_HH
#define ALICEPRIMARY_HH
#include <Rivet/Projections/ParticleFinder.hh>
#include <Rivet/Tools/Cuts.hh>

namespace Rivet
{
    /** 
     * @defgroup alice_rivet_primary Primary particle definition 
     * @ingroup alice_rivet
     */
    /** 
     * A Rivet projection that projects out primary particles -
     * according to the ALICE definition - from an event.  The
     * projection filters all charge states.  If one needs to have
     * only charged particles one need to apply another projection on
     * top of this one.
     * 
     * This version is for Rivet version 2 and higher, which allows for
     * the use of a Cut class and the base class ParticleFinder.
     * 
     * @ingroup alice_rivet_primary
     * @ingroup alice_rivet_proj
     */
    class Primary : public ParticleFinder
    {
    public:
      /** 
       * Consturctor 
       * 
       */
      Primary(const Cut& c=Cuts::open(), int pdg=0)
	: ParticleFinder(c),
	  _pdg(pdg)
      {
	setName("AlicePrimary");
      }
      /** 
       * Copy constructor 
       * 
       * @param o Object to copy from 
       */
      Primary(const Primary& o)
	: ParticleFinder(o),
	  _pdg(o._pdg)
      {}
      /** 
       * Destructor 
       */
      virtual ~Primary() {}
      /** 
       * @{ 
       * @name Projection interface 
       */
      /** 
       * Clone this projection object 
       * 
       * @return Copy of this projection object allocated on the heap
       */
      virtual std::unique_ptr<Projection> clone() const
      {
	return std::unique_ptr<Projection>(new Primary(*this));
      }
      /** 
       * Compare this projection to some other projection.  If the other
       * projection is also a Primary projection, then return @c 0,
       * otherwise @c -1.
       * 
       * @param p Projection to compare to 
       * 
       * @return 0 if @a p is an Primary, @c -1 otherwise 
       */
      virtual int compare(const Projection& p) const;
      /** 
       * Project out the primary particles of the passed event record @a
       * e.  This is the interface that does the actual projection.
       * 
       * @param e Event record. 
       */
      virtual void project(const Event& e);
      /* @} */
      /**
       * @{ 
       * @name Internal functions used 
       */
      /** 
       * Check if a particle is a primary according to the ALICE
       * definition.
       * 
       * @param p Particle to test 
       * @param pdg (optional) test for this specific PDG rather than
       * if @a p is long-lived or not.  Note, ancestors are still
       * checked if they are long-lived.
       * 
       * @return true if the particle is considered a primary, false
       * otherwise.
       */
      static bool isPrimary(const HepMC::GenParticle* p,
			    int                       pdg=0);
      /** 
       * Check if particle @a p has a status code that we should
       * ignore.  That is, what the HepMC standard defines as comment
       * lines (0, but not 3 as Pythia6 uses that for beam particles)
       * or as model specific codes (between 11 and 200).  Note,
       * transport specific codes (>200) are _not_ ignores.
       * 
       * @param p Particle to query 
       * 
       * @return true if the particle's status fulfills the above.
       */
      static bool mustIgnore(const HepMC::GenParticle* p);
      /** 
       * Check if a particle is of a long-lived (i.e., @f$ c\tau>1cm@f$)
       * species.
       * 
       * @param p Particle to test 
       * 
       * @return true if the particle is of a long-lived species 
       */
      static bool isLongLived(const HepMC::GenParticle* p);
      /** 
       * Check if this particle has decayed
       * 
       * @param p Particle to test 
       * 
       * @return true if the particle has decayed (status = 2)
       */
      static bool hasDecayed(const HepMC::GenParticle* p);
      /** 
       * Check if the particle @a p is a beam particle.  The standard
       * says beam particles have status 4, but Pythia6 uses status
       * code 3, so we check for that too.
       * 
       * @param p Particle
       * 
       * @return true if marked as beam particle 
       */
      static bool isBeam(const HepMC::GenParticle* p);
      /** 
       * Get the immediate ancestor of a particle 
       * 
       * @param p The particle to get the ancestor for 
       * 
       * @return Ancestor particle or null 
       */
      static const HepMC::GenParticle* ancestor(const HepMC::GenParticle* p);
      /** 
       * Get the immediate ancestor of a particle that is _not_ ignored. 
       * 
       * @param p The particle to get the ancestor for 
       * 
       * @return Ancestor particle or null 
       */
      static const HepMC::GenParticle* ancestor(const HepMC::GenParticle* p,
						bool);
      /* @} */
      /** 
       * Static access to log object 
       *
       * @return log object 
       */
      static Log& getLog()
      {
	return Log::getLog("Rivet.Projection.AlicePrimary");
      }
    protected:
      /** Possible particle type to test for */
      int _pdg = 0;
    };
}
#endif
//
// EOF
//
