// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include<string>
#include<iostream>
#include "Centrality.h"

namespace Rivet {
// Atlas PbPb  @ sqrt(s) = 2.76 TeV spectra from:
//  http://inspirehep.net/record/1360290/
//  https://hepdata.net/record/ins1360290
  class ATLAS_2015_I1360290 : public Analysis {
  public:

    /// Constructor
    ATLAS_2015_I1360290()
      : Analysis("ATLAS_2015_I1360290")
    {    }


    /// @ame Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      ChargedFinalState cfs(Cuts::eta > -2. && Cuts::eta < 2. && Cuts::pT > 0.5*GeV && Cuts::pT < 150.0*GeV);
      FinalState fcal(Cuts::abseta > 3.2 && Cuts::abseta < 4.9 && Cuts::pT > 0.1*GeV);
      FinalState minBiasB(Cuts::eta > 2.09 && Cuts::eta < 3.84 && Cuts::pT > 0.1*GeV);
      FinalState minBiasF(Cuts::eta < -2.09 && Cuts::eta > -3.84 && Cuts::pT > 0.1*GeV);
      addProjection(cfs,"CFS");
      addProjection(fcal,"CFSF");
      addProjection(minBiasB,"MBB");
      addProjection(minBiasF,"MBF");
      
      Histo1DPtr hist1 = bookHisto1D("etaCent1",400,0.0,4200.0);
      Histo1DPtr hist2 = bookHisto1D("etaCent2",400,0.0,4200.0);
      Histo1DPtr hist3 = bookHisto1D("etaCent3",400,0.0,4200.0);
      Histo1DPtr hist4 = bookHisto1D("etaCent4",400,0.0,4200.0);

      eTForward = bookHisto1D("eTForward",100,0.,4200.0);
      
      Histo1DPtr hist5 = bookHisto1D("ptCent",400,0.0,4200.0);
      caEta1 = CentralityHistogram<double>("etaCent1",400,0.0,4200.0,hist1);
      caEta2 = CentralityHistogram<double>("etaCent2",400,0.0,4200.0,hist2);
      caEta3 = CentralityHistogram<double>("etaCent3",400,0.0,4200.0,hist3);
      caEta4 = CentralityHistogram<double>("etaCent4",400,0.0,4200.0,hist4);
      capT1 = CentralityHistogram<double>("pTCent1",400,0.0,4200.0,hist5);
      taa = {1./26.3, 1./20.6, 1./14.4, 1./8.73, 1./5.05, 1./2.70, 1./1.34, 1./0.41};
      vector<double>centData = {0.00, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8};
      for(size_t i = 0; i < centData.size() - 1; ++i){
	// eta hists starts from table 55 ( first 1.7 < pT < 2.0)
      	caEta1.addHistogram(bookHisto1D(55 + i, 1, 1),centData[i],centData[i+1]);
	// From table 64, 6.7 < pT < 7.7
	caEta2.addHistogram(bookHisto1D(64 + i, 1, 1 ), centData[i],centData[i+1]);
	// From table 73, 19.9 < pT < 22.8
	caEta3.addHistogram(bookHisto1D(73 + i, 1, 1 ), centData[i],centData[i+1]);
	// From table 82, 59.8 < pT < 94.8
	caEta4.addHistogram(bookHisto1D(82 + i, 1, 1 ), centData[i],centData[i+1]);
	// pt hists starts from table 2 on hepmc, |eta| < 2.0
	capT1.addHistogram(bookHisto1D(2 + i, 1, 1),centData[i],centData[i+1]);
      }
      
     }
      


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const double weight = event.weight();
      const ChargedFinalState& parts = applyProjection<ChargedFinalState>(event,"CFS");
      const FinalState& forw = applyProjection<FinalState>(event,"CFSF");
      const FinalState& mbf = applyProjection<FinalState>(event,"MBF");
      const FinalState& mbb = applyProjection<FinalState>(event,"MBB");

      if(mbb.particles().size() < 1 || mbf.particles().size() < 1)
	      vetoEvent;

      double sumEt = 0.0;
      for(const Particle& p : forw.particles())
	     sumEt+=p.Et();
      eTForward->fill(sumEt,weight);
      caEta1.eventFill(weight,sumEt);
      caEta2.eventFill(weight,sumEt);
      caEta3.eventFill(weight,sumEt);
      caEta4.eventFill(weight,sumEt);
      capT1.eventFill(weight,sumEt);
      for (const Particle& p : parts.particles()){
	        double pT = p.pT();
		double eta = abs(p.eta());
      		if(pT > 1.7 && pT < 2.0) caEta1.fill(eta,weight/2.0,sumEt);
      		if(pT > 6.7 && pT < 7.7) caEta2.fill(eta,weight/2.0,sumEt);
      		if(pT > 19.9 && pT < 22.8) caEta3.fill(eta,weight/2.0,sumEt);
      		if(pT > 59.8 && pT < 94.8) caEta4.fill(eta,weight/2.0,sumEt);
      		if(eta < 2) capT1.fill(pT,weight/2/M_PI/pT/4.0,sumEt);
      }
     
    }


    /// Normalise histograms etc., after the run
    void finalize() {
	    caEta1.finalize();
	    //caEta1.scaleAll(taa);
	    caEta2.finalize();
	    //caEta2.scaleAll(taa);
	    caEta3.finalize();
	    //caEta3.scaleAll(taa);
	    caEta4.finalize();
	    //caEta4.scaleAll(taa);
	    capT1.finalize();
	    capT1.scaleAll(taa);
	    eTForward->scaleW(1./sumOfWeights());
	    

    }

    //@}


  private:
	CentralityHistogram<double> caEta1;
	CentralityHistogram<double> caEta2;
	CentralityHistogram<double> caEta3;
	CentralityHistogram<double> caEta4;
	CentralityHistogram<double> capT1;
	vector<double> taa;
	Histo1DPtr eTForward;

  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAS_2015_I1360290);


}
