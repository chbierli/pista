#! /usr/bin/env/python

usage = """Library for storing initial state of heavy ion events, primarily in impact parameter space.
ImpactMC defines a series of classes to use for interfacing Glauber-style calculations to other
programs, according to the standard defined in arXiv:XXXX.XXXX.
Usage: Library in other (Python) programs. Please cite the above reference when using this
interface.

Impact MC is licenced under the GNU GPL version 2. Please respect the MCnet Guidelines
found at: http://www.montecarlonet.org/GUIDELINES.

Lund University, 2017.
Contact: Johannes Bellm (johannes.bellm@thep.lu.se), Christian Bierlich (christian.bierlich@thep.lu.se).
"""

# Base classes: LineBase and BlockBase
######################################
# Base class for blocks of objects, a wrapper around a dict.
class BlockBase:
    memberNames = []
    def __init__(self, data = None):
        if data == None:
            self.data = {}
        else:
            self.data = data

    def __setitem__(self, key, item):
        self.data[key] = item

    def __getitem__(self, key):
        if key in self.data:
            return self.data[key]
        print "Block does not contain "+key
        return None


def test(self):
        for m in self.memberNames:
            if m not in self.data:
                raise KeyError("Member "+m+" not found in subcollision.")

# Base class for objects holding data, which fits
# on a single line in an input file.
class LineBase(BlockBase):
    # "Virtual" i/o methods which should always be implemented in derived classes.
    def readFromLine(self, line):
        raise NotImplementedError("Method readFromLine not implemented in derived class.")

    def printToLine(self):
        raise NotImplementedError("Method printToLine not implemented in derived class.")


# Classes defining the building blocks of the standard:
# Derived from LineBase: Particle, SubCollision and EnergyMomentumBin
# Derived from BlockBase: Init, Nuclei, Nucleons, Partons, SubCollisions, EnergyMomentumTensor-

# Derived class particles defines i/o and some simple operations on data. Particles go into all
# three particle blocks.
import math
class Particle(LineBase):
    memberNames = ['index','pid','bx','by','bz','px','py','pz','E']
    def readFromLine(self, line):
        sline = line.split()
        if len(sline) != 9:
            raise IOError("Tried to read a particle from a line with "+str(len(sline))+" entries.")
        for num,m in enumerate(self.memberNames):
            if m == 'index' or m == 'pid':
                self.data[m] = int(sline[num])
            else:
                self.data[m] = float(sline[num])

    def printToLine(self):
        s = ""
        for m in self.memberNames:
            s+=str(self.data[m])
            if m != "E":
                s+=" "
        return s

    # Helper functions for calculations
    # Distance in transverse plane between one particle and another.
    def dPerp(self, other):
        dx = (self["bx"] - other["bx"])**2
        dy = (self["by"] - other["by"])**2
        return math.sqrt(dx + dy)

    # Distance in 3d between one particle and another.
    def dist(self,other):
        dx = (self["bx"] - other["bx"])**2
        dy = (self["by"] - other["by"])**2
        dz = (self["bz"] - other["bz"])**2
        return math.sqrt(dx + dy + dz)

    # Shift particle by distance and angle
    def shift(self, b, angle):
        self["bx"]+=b*math.cos(angle)
        self["by"]+=b*math.sin(angle)

# The subcollision class defines a collision between two particles.
class SubCollision(LineBase):
    memberNames = ['i1','i2','type','scale']
    def readFromLine(self, line):
        sline = line.split()
        if len(sline) != 4:
            raise IOError("Tried to read a subcollision from a line with "+str(len(sline))+" entries.")
        for num,m in enumerate(self.memberNames):
            if m == 'scale':
                self.data[m] = float(sline[num])
            else:
                self.data[m] = int(sline[num])

    def printToLine(self):
        s = ""
        for m in self.memberNames:
            s+=str(self.data[m])
            if m != "scale":
                s+=" "
        return s


    # Helper methods to get properties of a given subcollision

# Init holds all initialization information about a list of events.
class Init(BlockBase):
    memberNames = ['version','model','energy','length']

# Nuclei is a list of particles corresponding to the largest structure (the nuclei) in a collision.
class Nuclei(BlockBase):
    pass
# Nucleons ...
class Nucleons(BlockBase):
    pass
# Partons...
class Partons(BlockBase):
    pass
# Subcollisions...
class SubCollisions(BlockBase):
    pass

# Event class holds all the blocks
class Event:
    def __init__(self):
        self.weight = 1.
        self.init = Init()
        self.nuclei = Nuclei()
        self.nucleons = Nucleons()
        self.partons = Partons()
        self.subcollisions = SubCollisions()

