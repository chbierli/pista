# BEGIN PLOT /ATLAS_2015_I1386475/d02-x01-y01
Title=Centrality 60-90\%
YLabel=$dN/d\eta$
LogY=0
# END PLOT
# BEGIN PLOT /ATLAS_2015_I1386475/d02-x01-y02
Title=Centrality 40-60\%
YLabel=$dN/d\eta$
LogY=0
# END PLOT
# BEGIN PLOT /ATLAS_2015_I1386475/d02-x01-y03
Title=Centrality 30-40\%
YLabel=$dN/d\eta$
LogY=0
# END PLOT
# BEGIN PLOT /ATLAS_2015_I1386475/d02-x01-y04
Title=Centrality 20-30\%
YLabel=$dN/d\eta$
LogY=0
# END PLOT
# BEGIN PLOT /ATLAS_2015_I1386475/d02-x01-y05
Title=Centrality 10-20\%
YLabel=$dN/d\eta$
LogY=0
# END PLOT
# BEGIN PLOT /ATLAS_2015_I1386475/d02-x01-y06
Title=Centrality 5-10\%
YLabel=$dN/d\eta$
LogY=0
# END PLOT
# BEGIN PLOT /ATLAS_2015_I1386475/d02-x01-y07
Title=Centrality 1-5\%
YLabel=$dN/d\eta$
LogY=0
# END PLOT
# BEGIN PLOT /ATLAS_2015_I1386475/d02-x01-y08
Title=Centrality 0-1\%
YLabel=$dN/d\eta$
LogY=0
# END PLOT
